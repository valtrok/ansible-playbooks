# Ansible playbooks

The Ansible playbooks I use to setup my homelab(s)

## log2ram setup

- Install and configure `log2ram`

## Post Debian install

- Install and configure `zram`
- Install and configure `snapper`

## Post Proxmox install

- Change `apt` repositories to the no-subscription ones
- Enable PCIe Passthrough
- Apply various tweaks to minimize SSD wearout

## SSH hardening

- Configure `sshd` to be _a bit_ more secure

## UPS management

- Install and configure `apcupsd` to allow communication with my UPS
